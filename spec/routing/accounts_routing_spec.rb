require 'rails_helper'

RSpec.describe AccountsController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/accounts/1').to route_to('accounts#show', id: '1')
    end

    it 'routes to #transfer' do
      expect(post: '/accounts/1/transfer').to route_to('accounts#transfer', source_account_id: '1')
    end
  end
end
