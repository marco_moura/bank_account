require 'rails_helper'

RSpec.describe TransactionBuilder do
  context '.attributes' do
    subject { described_class.new opts }

    let(:opts) do
      {
        'destination_account_id' => 1,
        'source_account_id' => 2,
        'amount' => 3
      }
    end

    let(:result) do
      [
        { amount: 3.0, account_id: 1, kind: :credit },
        { amount: 3.0, account_id: 2, kind: :debit }
      ]
    end

    it { expect(subject.attributes).to match_array result }
  end
end
