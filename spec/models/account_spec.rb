require 'rails_helper'

RSpec.describe Account, type: :model do
  it { is_expected.to validate_presence_of :balance }
  it { is_expected.to validate_numericality_of :balance }
end
