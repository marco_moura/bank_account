require 'rails_helper'

RSpec.describe Transaction, type: :model do
  it { is_expected.to validate_presence_of :account }
  it { is_expected.to validate_length_of :amount }
  it { is_expected.to validate_numericality_of :amount }
  it { is_expected.to define_enum_for(:kind).with %i[credit debit] }
end
