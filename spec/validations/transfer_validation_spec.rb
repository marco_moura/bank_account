require 'rails_helper'

RSpec.describe TransferValidation, type: :model do
  subject { described_class.new double.as_null_object }
  it { is_expected.to validate_presence_of :source_account_id }
  it { is_expected.to validate_presence_of :destination_account_id }
  it { is_expected.to validate_presence_of :amount }
  it { is_expected.to validate_numericality_of :amount }
end
