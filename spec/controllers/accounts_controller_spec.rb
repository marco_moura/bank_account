require 'rails_helper'

RSpec.describe AccountsController, type: :controller do
  describe 'GET #show' do
    context 'success' do
      before do
        allow(AccountFacade).to receive(:show).and_return({})
        get :show, params: { id: 171 }
      end

      it { expect(response).to have_http_status(:ok) }
      it { expect(response.body).to eq '{}' }
    end

    context 'error' do
      before do
        allow(AccountFacade).to receive(:show).and_raise(AccountFacade::NotFoundError)
        get :show, params: { id: 171 }
      end

      let(:error_body) do
        '{"errors":[{"message":"Account does not exist"}]}'
      end

      it { expect(response).to have_http_status(:not_found) }
      it { expect(response.body).to eq error_body }
    end
  end

  describe 'POST #transfer' do
    context 'success' do
      before do
        allow(AccountFacade).to receive(:transfer).and_return({})
        get :transfer, params: { source_account_id: 171 }
      end

      it { expect(response).to have_http_status :no_content }
    end

    context 'error' do
      context 'invalid params' do
        before do
          allow(AccountFacade).to receive(:transfer)
            .and_raise(AccountFacade::ParameterInvalidError, error)
          get :transfer, params: { source_account_id: 171 }
        end

        let(:error) do
          double(errors: double(full_messages: ['Source account can\'t be blank']))
        end
        let(:error_body) do
          "{\"errors\":[{\"message\":\"Source account can't be blank\"}]}"
        end

        it { expect(response).to have_http_status :unprocessable_entity }
        it { expect(response.body).to eq error_body }
      end
    end
  end
end
