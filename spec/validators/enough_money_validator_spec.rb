require 'rails_helper'

class FakeEnoughMoneyValidator
  include ActiveModel::Validations
  attr_reader :amount, :account
  def initialize(account)
    @amount = 10
    @account = account
  end
  validates :account, enough_money: true
end

RSpec.describe 'EnoughMoneyValidator' do
  subject { FakeEnoughMoneyValidator.new(double(balance: balance)) }

  describe 'when account balance is' do
    describe 'greater than amount' do
      let(:balance) { 11 }
      it { is_expected.to be_valid }
    end

    describe 'equal than amount' do
      let(:balance) { 10 }
      it { is_expected.to be_valid }
    end

    describe 'lesser than amount' do
      let(:balance) { 9 }
      it { is_expected.not_to be_valid }
    end
  end
end
