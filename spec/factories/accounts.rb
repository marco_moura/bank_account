FactoryGirl.define do
  factory :account do
    balance 0
    association :client

    factory :payer_account do
      balance 10
      association :client, factory: :payer
    end

    factory :receiver_account do
      balance 0
      association :client, factory: :receiver
    end
  end
end
