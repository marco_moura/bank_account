FactoryGirl.define do
  factory :client do
    factory :receiver do
      id 1
      name 'The wolf receiver'
    end

    factory :payer do
      id 2
      name 'The fearless Payer'
    end
  end
end
