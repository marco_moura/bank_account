FactoryGirl.define do
  factory :transaction do
    account
    amount 9.99
    kind :credit
  end
end
