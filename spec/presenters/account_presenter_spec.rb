require 'rails_helper'

RSpec.describe AccountPresenter do
  context '#balance' do
    let(:fake_helper) { spy('helper') }
    before { described_class.balance(1, fake_helper) }

    it do
      expect(fake_helper).to have_received(:number_to_currency)
        .with(1, unit: 'R$', separator: ',', delimiter: '')
    end
  end

  context '.to_json' do
    subject { described_class.new('R$10,00').to_json }
    let(:json) { '{"account":{"balance":"R$10,00"}}' }

    it { is_expected.to eql json }
  end
end
