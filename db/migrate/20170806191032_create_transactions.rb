class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.references :account
      t.decimal :amount
      t.integer :kind

      t.timestamps
    end
  end
end
