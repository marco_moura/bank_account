class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.references :client
      t.decimal :balance, default: 0.0

      t.timestamps
    end
  end
end
