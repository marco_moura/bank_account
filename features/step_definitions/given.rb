Given(/^the client has in his account a balance of (\d+)$/) do |balance|
  FactoryGirl.create :receiver_account, balance: balance
end

Given(/^an account payer has a balance of (\d+)$/) do |balance|
  @payer_account = FactoryGirl.create :payer_account, balance: balance
end

Given(/^an account receiver has a balance of (\d+)$/) do |balance|
  @receiver_account = FactoryGirl.create :receiver_account, balance: balance
end
