Then(/^the status should be (\d+)$/) do |code|
  expect(last_response.status).to eq code
end

Then(/^the body should be:$/) do |result|
  expect(JSON.parse(last_response.body)).to eq JSON.parse(result)
end

Then(/^the (.*)'s balance should be (\d+)$/) do |type, balance|
  steps %(
    When the #{type} requests balance
     Then the body should be:
    """
      {
        "account": {
          "balance": "R$#{balance},00"
        }
      }
    """
  )
end
