When(/^the client requests balance$/) do
  get_account(1)
end

When(/^the payer requests balance$/) do
  get_account(@payer_account.client.id)
end

When(/^the receiver requests balance$/) do
  get_account(@receiver_account.client.id)
end

When(/^the account Payer transfer (\d+) to account Receiver$/) do |amount|
  params = { destination_account_id: @receiver_account.id, amount: amount }

  post "accounts/#{@payer_account.id}/transfer", params
end

When(/^the account Payer transfer (\d+) to a no\-existent account$/) do |amount|
  params = { destination_account_id: 171, amount: amount }

  post "accounts/#{@payer_account.id}/transfer", params
end

When(/^the account Payer transfer without inform the destination account and amount$/) do
  post "accounts/#{@payer_account.id}/transfer"
end
