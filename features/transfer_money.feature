Feature: Transfer Money
  Clients has an account and can make transfers to other clients accounts.

Background:
  Given an account payer has a balance of 10
    And an account receiver has a balance of 0

Scenario: Success
  When the account Payer transfer 5 to account Receiver
  Then the status should be 204
   And the payer's balance should be 5
   And the receiver's balance should be 5

Scenario: Fails when not enought money
  When the account Payer transfer 11 to account Receiver
  Then the status should be 412
   And the body should be:
  """
  {
    "errors": [
      {
        "message": "Account not enough money"
      }
    ]
  }
  """

Scenario: Fails when some params is missing
  When the account Payer transfer without inform the destination account and amount
  Then the status should be 422
   And the body should be:
  """
  {
    "errors": [
      {
        "message": "Destination account can't be blank"
      },
      {
        "message": "Amount can't be blank"
      }
    ]
  }
  """

Scenario: Fails when recipient not found
  When the account Payer transfer 5 to a no-existent account
  Then the status should be 412
   And the body should be:
  """
  {
    "errors": [
      {
        "message": "Account 171 does not exist"
      }
    ]
  }
  """
