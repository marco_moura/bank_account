Feature: Get current balance
  As a client
  I want to ask the current balance of my account.

Scenario: Success
  Client requests balance
  System calculates the current balance of the account

  Given the client has in his account a balance of 10
  When the client requests balance
  Then the status should be 200
   And the body should be:
  """
  {
    "account": {
      "balance": "R$10,00"
    }
  }
  """

Scenario: Fails, account does not exists
  If the account does not exist
  System deliveries a message telling that the account does not exist

  When the client requests balance
  Then the status should be 404
   And the body should be:
  """
  {
    "errors": [
      {
        "message": "Account does not exist"
      }
    ]
  }
  """
