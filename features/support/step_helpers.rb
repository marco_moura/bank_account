Transform(/(\d+)$/) do |number|
  number.to_i
end

module StepHelpers
  def get_account(id)
    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    get "accounts/#{id}"
  end
end

World(StepHelpers)
