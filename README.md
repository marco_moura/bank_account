[![Build Status](https://semaphoreci.com/api/v1/marcomoura/bank_account/branches/master/badge.svg)](https://semaphoreci.com/marcomoura/bank_account)
# API Bank Account

## How to use

## Prepare the application

To run the app you need to:
- install the dependencies
- prepare the demostration data
- start application

To do this run the following commands
```bash
bundle
rails db:setup
rails db:seed
rails start
```
## Use Case #1 - Transfer Money

### Client triggers “Transfer Money” with the input described above.
```bash
# check balance
$ curl -v http://localhost:3000/accounts/1
< HTTP/1.1 200 OK
< {"account":{"balance":"R$10,00"}}

# transfer
$ curl -v http://localhost:3000/accounts/1/transfer -d 'destination_account_id=2&amount=10'
< HTTP/1.1 204 No Content

$ curl -v http://localhost:3000/accounts/1
< HTTP/1.1 200 OK
< {"account":{"balance":"R$0,00"}}

$ curl -v http://localhost:3000/accounts/2
< HTTP/1.1 200 OK
< {"account":{"balance":"R$10,00"}}

$ curl -v http://localhost:3000/accounts/2/transfer -d 'destination_account_id=1&amount=5'
< HTTP/1.1 204 No Content
```


### Exception Course: Not enough money on the source account.
```bash
$ curl http://localhost:3000/accounts/1/transfer -d 'destination_account_id=2&amount=20'
< HTTP/1.1 412 Precondition Failed
< {"errors":[{"message":"Account not enough money"}]}

$ curl http://localhost:3000/accounts/1/transfer -d 'destination_account_id=2&amount=0'
< HTTP/1.1 422 Unprocessable Entity
< {"errors":[{"message":"Amount must be greater than 0"}]}

$ curl -v http://localhost:3000/accounts/1/transfer -d '&amount=10'
< HTTP/1.1 422 Unprocessable Entity
{"errors":[{"message":"Destination account can't be blank"}]}
```

## Use case #2 - Get current balance

### Primary course (happy path):

```bash
$ curl -v http://localhost:3000/accounts/1
< HTTP/1.1 200 OK
< {"account":{"balance":"R$10,00"}}

$ curl -v http://localhost:3000/accounts/2
< HTTP/1.1 200 OK
< {"account":{"balance":"R$0,00"}}
```

OBS: if you got a error message on the happy path, maybe you forgot to run `rails db:seed`

### Exception course: Account does not exist

```bash
$ curl http://localhost:3000/accounts/100
=> {"error":{"message":"Account does not exist"}}
```

## Teste suite
To execute the test suite, run:
```bash
rails spec
cucumber
```

## Code coverage

```bash
cd project_folder
google-chrome coverage/index.html
```

## Context

Create a banking accounting system where each client has an account and can make transfers to other clients accounts.
The client can also ask the current balance of his account.
Consider that this bank only handles one type of currency that is brazilian Real (BRL).
The system must have an interface, it can be an API, a CLI or a web interface (choose one).

## Use Case #1 - Transfer Money

Input:
  `<source_account_id>, <destination_account_id>, <amount>`

Primary course (happy path):

  1. Client triggers “Transfer Money” with the input described above.

  2. System validates all data.

  3. System creates a debit on the source account.

  4. System creates a credit on the destination account.

Exception Course: Not enough money on the source account.

  1. System cancels the transfer.

## Use case #2 - Get current balance

Input:
  `<account_id>`

Primary course (happy path):
  1. Client triggers “Get balance” with the input described above.

  2. System calculates the current balance of the account.

Exception course: Account does not exist
  1. System deliveries a message telling that the account does not exist.


## README

Be sure to write a clear README as it was an open source project.

Focus on a new developer joining your project,
what are the main things to understand before diving into the code itself?