class AccountPresenter
  def self.do(account)
    new balance(account.balance, ActionController::Base.helpers)
  end

  def self.balance(value, view)
    view.number_to_currency value, unit: 'R$', separator: ',', delimiter: ''
  end

  attr_reader :account

  def initialize(balance)
    @account = { balance: balance }
  end
end
