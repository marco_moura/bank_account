class TransferValidation < SimpleDelegator
  include ActiveModel::Validations

  attr_accessor :source_account_id, :destination_account_id, :amount

  def initialize(attributes)
    @source_account_id = attributes['source_account_id']
    @destination_account_id = attributes['destination_account_id']
    @amount = attributes['amount']
  end

  validates :source_account_id, :destination_account_id, :amount,
            presence: true

  validates :amount, numericality: { allow_blank: :true, greater_than: 0 }
end
