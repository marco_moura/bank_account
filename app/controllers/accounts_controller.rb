class AccountsController < ApplicationController
  # GET /account/1
  def show
    account = AccountFacade.show(params.require(:id))
    render json: account
  rescue AccountFacade::NotFoundError
    render json: t('.not_found'), status: :not_found
  end

  # POST /account/1/transfer
  def transfer
    AccountFacade.transfer(transfer_params)
  rescue AccountFacade::ParameterInvalidError => e
    render json: { errors: e.errors }, status: :unprocessable_entity
  rescue AccountFacade::InsufficientBalanceError,
         AccountFacade::AccountNotExistError => e
    render json: { errors: e.errors }, status: :precondition_failed
  end

  private

  def transfer_params
    params.permit(:source_account_id, :destination_account_id, :amount)
  end
end
