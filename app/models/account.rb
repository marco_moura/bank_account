class Account < ApplicationRecord
  has_many :transactions, inverse_of: :account, dependent: :destroy
  belongs_to :client, inverse_of: :account

  validates :balance, presence: true, numericality: { allow_blank: true }
end
