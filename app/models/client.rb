class Client < ApplicationRecord
  has_one :account, inverse_of: :client, dependent: :destroy
  validates :name, presence: true, length: { maximum: 255 }

  accepts_nested_attributes_for :account
end
