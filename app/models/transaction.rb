class Transaction < ApplicationRecord
  belongs_to :account, inverse_of: :transactions

  enum kind: %i[credit debit]

  validates :amount,
            presence: true,
            numericality: { allow_blank: true }
  validates :account,
            presence: true,
            enough_money: { allow_blank: :true, if: :debit? }
end
