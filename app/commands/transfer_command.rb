class TransferCommand
  def self.execute(opts)
    attributes = TransactionBuilder.attributes(opts)
    new(attributes).execute
  end

  def initialize(attributes, model = Transaction)
    @attributes = attributes
    @model = model
  end

  def execute
    @model.transaction do
      @attributes.map do |attribute|
        transaction = @model.create!(attribute)

        balance = calculate_balance(attribute, transaction.account.balance)
        transaction.account.update balance: balance
      end
    end
  rescue ActiveRecord::RecordInvalid => error
    record = error.record
    raise AccountNotExistError, record if record.errors[:account].include? 'must exist'
    raise InsufficientBalanceError, record
  end

  private

  def calculate_balance(attributes, balance)
    amount = attributes[:amount]
    attributes[:kind].eql?(:credit) ? balance + amount : balance - amount
  end
end
