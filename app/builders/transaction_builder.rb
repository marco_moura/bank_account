class TransactionBuilder
  def self.attributes(opts)
    new(opts).attributes
  end

  def initialize(opts)
    @opts = opts
  end

  def attributes
    [
      hash(@opts['destination_account_id'], :credit),
      hash(@opts['source_account_id'], :debit)
    ]
  end

  private

  def hash(account, kind)
    {
      amount: @opts['amount'].to_f,
      account_id: account,
      kind: kind
    }
  end
end
