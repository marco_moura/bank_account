class EnoughMoneyValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if record.amount <= value.balance

    record.errors[attribute] << (options[:message] || I18n.t('errors.messages.enough_money'))
  end
end
