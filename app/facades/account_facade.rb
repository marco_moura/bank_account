# This is a facade for account's crud
module AccountFacade
  def self.show(id)
    account = Account.find(id)
    AccountPresenter.do(account)
  rescue ActiveRecord::RecordNotFound
    raise AccountFacade::NotFoundError
  end

  def self.transfer(opts)
    validator = TransferValidation.new(opts)
    raise ParameterInvalidError, validator unless validator.valid?

    TransferCommand.execute(opts)
  rescue TransferCommand::InsufficientBalanceError => e
    raise AccountFacade::InsufficientBalanceError, e.errors
  rescue TransferCommand::AccountNotExistError => e
    raise AccountFacade::AccountNotExistError, e.errors
  end
end
