class TransferCommand
  class InsufficientBalanceError < StandardError
    attr_reader :errors

    def initialize(record)
      @errors = record.errors.full_messages.collect { |error| { message: error } }
    end
  end
end
