class TransferCommand
  class AccountNotExistError < StandardError
    attr_reader :errors

    def initialize(record)
      @errors = [{ message: error_message(record.account_id) }]
    end

    private

    def error_message(id)
      I18n.t('errors.messages.account_not_exist_error', id: id)
    end
  end
end
