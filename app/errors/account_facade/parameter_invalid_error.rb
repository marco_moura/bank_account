module AccountFacade
  class ParameterInvalidError < StandardError
    attr_reader :errors

    def initialize(validator)
      @errors = validator.errors.full_messages.collect { |error| { message: error } }
    end
  end
end
