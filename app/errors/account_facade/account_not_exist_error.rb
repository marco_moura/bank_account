module AccountFacade
  class AccountNotExistError < ActiveRecord::ActiveRecordError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end
  end
end
