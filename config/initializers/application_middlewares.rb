module BankAccounting
  class Application < Rails::Application
    config.middleware.delete ::Rack::Sendfile
  end
end
