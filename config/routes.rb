Rails.application.routes.draw do
  constraints format: :json do
    get 'accounts/:id', to: 'accounts#show'
    post 'accounts/:source_account_id/transfer', to: 'accounts#transfer'
  end
end
